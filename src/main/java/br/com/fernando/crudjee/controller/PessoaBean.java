package br.com.fernando.crudjee.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.fernando.crudjee.dao.PessoaDAO;
import br.com.fernando.crudjee.model.Pessoa;

@Named(value = "pessoaBean")
@RequestScoped
public class PessoaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Pessoa pessoa;
	private List<Pessoa> pessoas;

	@Inject
	private PessoaDAO pessoaDAO;

	@PostConstruct
	public void init() {
		pessoa = new Pessoa();
		recuperarPessoas();
	}

	public String salvar() {
		if (pessoa != null) {
			if (pessoa.getId() == null) {
				pessoaDAO.create(pessoa);
			} else {
				pessoaDAO.update(pessoa);
			}
		}
		novo();
		recuperarPessoas();
		return null;
	}

	public String novo() {
		pessoa = new Pessoa();
		return null;
	}

	public String excluir(Pessoa pessoa) {
		pessoaDAO.delete(pessoa);
		recuperarPessoas();
		return null;
	}

	public String alterar(Pessoa pessoa) {
		this.pessoa = pessoa;
		return null;
	}

	public void recuperarPessoas() {
		pessoas = pessoaDAO.get();
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public List<Pessoa> getPessoas() {
		return pessoas;
	}

	public void setPessoas(List<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}

}
