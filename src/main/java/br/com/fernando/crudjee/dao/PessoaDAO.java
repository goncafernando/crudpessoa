package br.com.fernando.crudjee.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.fernando.crudjee.model.Pessoa;

@Stateless
public class PessoaDAO {

	@PersistenceContext
	EntityManager em;

	public void create(Pessoa pessoa) {
		em.persist(pessoa);
	}

	public Pessoa update(Pessoa pessoa) {
		return em.merge(pessoa);
	}

	public void delete(Pessoa pessoa) {
		if (!em.contains(pessoa)) {
			pessoa = em.merge(pessoa);
		}
		em.remove(pessoa);
	}

	public List<Pessoa> get() {
		return em.createNamedQuery("Pessoa.findAll", Pessoa.class).getResultList();
	}
}
